# Tic Tac Toe

A minimal Tic Tac Toe using Ember.js + Ruby on Rails

It uses a simple websocket channel communication for allowing 2 players play.

- Ruby version 3.1.2

- Configuration => start `redis-server` on default port

### Install dependencies

- `bundle install`
- `rails ember:install`

### Execute server

- `rails s`
