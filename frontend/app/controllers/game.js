import { service } from '@ember/service';
import { action } from '@ember/object';
import Controller from '@ember/controller';
import ActionCable from 'actioncable';

export default class GameController extends Controller {
  @service('event-handler') eventHandler;

  constructor(...args) {
    super(...args);
    this.cable = ActionCable.createConsumer(`ws://${location.host}/cable`);
    this.subscription = {};
  }

  initialize() {
    const { matchId } = this.model;
    this.subscription = this.cable.subscriptions.create(
      { channel: 'GameChannel', matchId },
      {
        received: (data) => {
          const handleEvent = this.eventHandler[data.event];
          if (handleEvent) {
            handleEvent(this.model, data);
          }
        },
      }
    );
  }

  @action
  mark(row, column, symbol) {
    const { playerId, playerToMoveId, gameBoard, gameOver } = this.model;
    const board = gameBoard ? JSON.parse(gameBoard) : [];

    if (playerId !== playerToMoveId) return;
    if (board[row][column]) return;
    if (gameOver) return;

    this.subscription.perform('mark', { row, column, symbol });
  }
}
