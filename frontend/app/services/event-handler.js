import Service from '@ember/service';

export default class EventHandlerService extends Service {
  playerJoined(game, data) {
    if (game.playerId && game.playerId !== data.playerId) {
      console.log('Player has joined!');
    } else {
      game.join(data.playerId);
    }
  }

  startGame(game, data) {
    game.startGame(data);
  }

  playerLeft(game) {
    game.leave();
  }

  markPerformed(game, data) {
    game.updateBoard(data);
  }

  gameOver(game, data) {
    game.checkGameOver(data);
  }
}
