import Component from '@glimmer/component';
// import { tracked } from '@glimmer/tracking';
import { service } from '@ember/service';
import { action } from '@ember/object';

export default class GameBoardComponent extends Component {
  @service router;

  @action
  newGame() {
    location.reload();
  }

  get gameBoard() {
    return this.args.board ? JSON.parse(this.args.board) : [];
  }

  set gameBoard(currentBoard) {
    this.gameBoard = JSON.parse(currentBoard);
  }

  @action
  onClick(row, col, symbol) {
    this.args.markAction(row, col, symbol);
  }
}
