import Route from '@ember/routing/route';
import { service } from '@ember/service';

export default class GameRoute extends Route {
  @service store;

  model(params) {
    return this.store.createRecord('game', {
      matchId: params.id,
    });
  }

  setupController(controller, model) {
    controller.set('model', model);
    controller.initialize();
  }
}
