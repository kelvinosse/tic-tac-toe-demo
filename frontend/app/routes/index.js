import Route from '@ember/routing/route';
import { service } from '@ember/service';
import {
  uniqueNamesGenerator,
  adjectives,
  colors,
} from 'unique-names-generator';

export default class IndexRoute extends Route {
  @service router;

  beforeModel() {
    const roomName = uniqueNamesGenerator({
      dictionaries: [adjectives, colors],
    });
    this.router.transitionTo('game', roomName);
  }
}
