import Model, { attr } from '@ember-data/model';
import { set } from '@ember/object';

export const GameStatus = {
  ON_HOLD: 'ON_HOLD',
  ACTIVE: 'ACTIVE',
  FINISHED: 'FINISHED',
};

export default class GameModel extends Model {
  @attr('string') matchId;
  @attr('string') playerId;
  @attr('string') playerToMoveId;
  @attr('string') winnerMessage;
  @attr('string') gameStatus;
  @attr('string') symbolTurn;
  @attr('string') gameBoard;
  @attr('boolean') gameOver;
  @attr() players;

  startGame({ players, playerId }) {
    this.players = players;
    this.gameBoard = JSON.stringify([
      Array(3).fill(''),
      Array(3).fill(''),
      Array(3).fill(''),
    ]);
    this.setTurn(playerId, GameStatus.ACTIVE);
    this.symbolTurn = 'X';
  }

  join(playerId) {
    this.playerId = playerId;
    this.gameStatus = GameStatus.ON_HOLD;
  }

  leave() {
    this.gameStatus = GameStatus.ON_HOLD;
  }

  setTurn(playerToMoveId, gameStatus) {
    this.playerToMoveId = playerToMoveId;
    this.gameStatus = gameStatus;
  }

  updateBoard({ row, column, symbol, nextPlayerId }) {
    const board = Array.from(JSON.parse(this.gameBoard));
    board[row][column] = symbol;
    set(this, 'gameBoard', JSON.stringify(board));
    this.setTurn(nextPlayerId, GameStatus.ACTIVE);
    this.symbolTurn = this.symbolTurn === 'X' ? 'O' : 'X';
  }

  checkGameOver({ gameOver, winner }) {
    const winnerMessages =
      {
        X: 'Player "X" is the Winner',
        O: 'Player "O" is the Winner',
        Draw: 'The Game is a Tie',
      } || '';

    this.gameOver = gameOver;
    this.winnerMessage = winnerMessages[winner];
  }
}
