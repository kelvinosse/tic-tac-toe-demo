class Game
    def initialize(match_id)
        @match_id = match_id
        @current_players = []
    end

    def start_game()
        @game_board = Array.new(3) { Array.new(3, " ") }
        @current_player = @current_players.first
        ActionCable.server.broadcast("game_#{@match_id}", { event: "startGame", players: @current_players, playerId: @current_player })
    end

    def join(player_id)
        # do not continue if we have enough players on a game
        return if @current_players.length == 2

        @current_players.push(player_id)
        ActionCable.server.broadcast("game_#{@match_id}", { event: "playerJoined", playerId: player_id })

        start_game if @current_players.length == 2
    end

    def leave(player_id)
        return if @current_players.find_index.nil?
        @current_players.delete(player_id)
        ActionCable.server.broadcast("game_#{@match_id}", { event: "playerLeft", playerId: player_id })
    end

    def mark(row, column, symbol)
        @game_board[row][column] = symbol
        @current_player = @current_player == @current_players.first ? @current_players.last : @current_players.first

        ActionCable.server.broadcast("game_#{@match_id}", { event: "markPerformed", nextPlayerId: @current_player, symbol: symbol, row: row, column: column })

        ActionCable.server.broadcast("game_#{@match_id}", { event: "gameOver", gameOver: game_over?(), winner: winner?() })
    end

    def game_over?
        return true if win?("X") || win?("O") || draw?
        false
    end

    def winner?
        return "X" if win?("X")
        return "O" if win?("O")
        return "Draw" if draw?()
        return "None"
    end

    private

    def win?(mark)
    # Iteraring rows for finding a win
    (0..2).each do |row|
      return true if @game_board[row].all? { |cell| cell == mark }
    end

    # Iterating columns for finding a win
    (0..2).each do |column|
      columns = []
      (0..2).each do |row|
        columns << @game_board[row][column]
      end
      return true if columns.all? { |cell| cell == mark }
    end

    # Diagonals validation
    left_diagonal = [@game_board[0][0], @game_board[1][1], @game_board[2][2]]
    right_diagonal = [@game_board[0][2], @game_board[1][1], @game_board[2][0]]
    return true if left_diagonal.all? { |cell| cell == mark } || right_diagonal.all? { |cell| cell == mark }

    false
  end

  def draw?
    @game_board.flatten.none? { |cell| cell == " " }
  end

end
