class MatchMaking
    @@matches = {}

    def self.search_match(matchId)
        return @@matches[matchId]
    end

    def self.generate_match(matchId)
        @@matches[matchId] = Game.new(matchId)
    end
end
