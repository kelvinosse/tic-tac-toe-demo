module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :uniqueId

    #generate uuid on each new connect
    def connect
      self.uniqueId = SecureRandom.uuid
    end
  end
end
