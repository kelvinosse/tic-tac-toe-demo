class GameChannel < ApplicationCable::Channel
  def subscribed
    id = params[:matchId]
    stream_from "game_#{id}"

    game = MatchMaking.search_match(id)
    if game.nil?
      game = MatchMaking.generate_match(id)
    end

    game.join(uniqueId)
  end

  def unsubscribed
    id = params[:matchId]
    game = MatchMaking.search_match(id)
    game.leave(uniqueId)
  end

  def join
    id = params[:matchId]
    game = MatchMaking.search_match(id)
    game.join(uniqueId)
  end

  def mark(payload)
    id = params[:matchId]
    game = MatchMaking.search_match(id)
    row, column, symbol = payload.values_at("row", "column", "symbol")
    game.mark(row, column, symbol)
  end

end
